# Food Truck Finder Command Line Application

This is a simple command-line program that will print out a list of food trucks from the [San Francisco Data API](https://dev.socrata.com/foundry/data.sfgov.org/jjew-r69b), powered by Socrata. Given the time you run the program, it will provide you a list of open food trucks in the San Francisco area, along with its location.

## How it works
### Installation
#### Gradle
If you don't have Gradle already, please install Gradle by going to this [link](https://gradle.org/install/) given your environment.
#### Java SE
If you don't have Java runtime already, you can download it by going this [link](https://www.oracle.com/java/technologies/javase-downloads.html).

### Build the Project
After you have downloaded the zipped files, unzip the program, and navigate to the FoodTruckFinder root directory. Inside your terminal, run this command:
- `./gradlew build`

### Run the Program
After it has successfully finished building the project, you can run the program with the following command:
- `./gradlew run`

It will provide you the first 10 open Food Trucks at the time you run the program in alphabetical order. If you want to see more, please press enter. It will exit automatically if there are no more open food trucks. You're welcome to exit the program by typing `exit`.

## Demo
![Demo](https://cldup.com/P2HAYOpLjI.gif)

## Full Scale Web Application
![High Level Design](https://cldup.com/Rp3qcQnt8n-3000x3000.png)

At a high level, I want a system that is highly performant and scalable. I'd have multiple application servers to serve the requests with load balancers in front for traffic distribution. Considering there are millions of concurrent users, I don't want to call the Socrata API for every request, so I'd store the data (e.g. all food trucks in a given day) into a database. I'd choose a cloud-based NoSQL database like Cassandra because it's a cost savings solution that allows us to store large volumes of data and prioritize performance/scalability. I'd create a system api where requests are made to a service that queries the database and returns open food trucks given the specific day/time.

I imagine there are popular times (lunch/dinner), so I would cache hot queries. The application servers can quickly check the cache before hitting the database to help improve performance. I'd keep my data up to date by calling the Socrata API daily to update, delete, or add rows to our database. As we scale, indexing and possibly sharding could help.

Since our system is so read heavy, multiple secondary database servers could be designated for read traffic to provide us fault tolerance as the primary can failover to the secondary if the primary goes down. All the writes would go to the primary and then that would be replicated to the secondary servers.

For the front end, I'd want the ability to view food trucks in a user's current vicinity in list form and on a map. Users would be able to see all nearby food trucks via the Google Maps API given the user's location via manual user input or something like the Geolocation API. Users would be able to filter the list by distance and start/end times.
