package main.java.FoodTruckFinder;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import main.java.FoodTruckFinder.Establishment.EstablishmentType;
import main.java.FoodTruckFinder.Establishment.PrintEstablishmentList;
import main.java.FoodTruckFinder.Utils.CustomDate;

public class Driver {
	public static void main(String[] args) {
		// Get the current date/time in the SF timezone
		System.out.println("What type of establishments are you looking for?");
		System.out.println("Enter: (1) Food Trucks (2) Restaurants (3) Both");
		int i = 0;
		
		List<EstablishmentType> userInputTypes = new ArrayList<>();
		Scanner scan = new Scanner(System.in);
		
		while (i != 1 && i != 1 && i != 2 && i != 3) {
	
			System.out.println("Please enter (1) Food Trucks (2) Restaurants (3) Both");
			i = scan.nextInt();
			System.out.println(i);
		}
		if (i == 1) {
			userInputTypes.add(EstablishmentType.FOODTRUCK);
		} else if (i == 2) {
			userInputTypes.add(EstablishmentType.RESTAURANT);
		} else if (i == 3){
			userInputTypes.add(EstablishmentType.FOODTRUCK);
			userInputTypes.add(EstablishmentType.RESTAURANT);
		} 
		
		CustomDate date = new CustomDate();
				
		// Print Food Truck List based on User Input
		PrintEstablishmentList.printEstablishmentList(date.getDay(), date.getTime(), userInputTypes);
	}
}
