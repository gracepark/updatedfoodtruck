package main.java.FoodTruckFinder.Establishment.Restaurant;

import java.util.Arrays;
import java.util.List;

import main.java.FoodTruckFinder.Establishment.Establishment;
import main.java.FoodTruckFinder.Establishment.EstablishmentHTTPService;


public class RestaurantService implements EstablishmentHTTPService {
	
	@Override
	public String fetchData(String day, String time) {
		parseResponse("");
		return "";
	}

	@Override
	public List<Establishment> parseResponse(String response) {
		return Arrays.asList(new Restaurant());
	} 
}
