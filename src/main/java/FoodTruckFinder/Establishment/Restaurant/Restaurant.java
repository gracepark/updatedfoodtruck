package main.java.FoodTruckFinder.Establishment.Restaurant;

import main.java.FoodTruckFinder.Establishment.Establishment;

public class Restaurant implements Establishment {
	private String name;
	private String location;
	
	
	public Restaurant() {
		
	}

	public Restaurant(String name, String location) {
		this.name = name;
		this.location = location;
	}
	
	@Override
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String getLocation() {
		return this.location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
}
