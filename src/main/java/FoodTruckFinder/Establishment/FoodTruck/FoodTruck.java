package main.java.FoodTruckFinder.Establishment.FoodTruck;

import main.java.FoodTruckFinder.Establishment.Establishment;

/**
 * Represents a food truck with a Mobile Food Facility Permit
 * A food truck applicant can have multiple locations
 */
public class FoodTruck implements Establishment {
	private String applicant;
	private String location;
	
	
	public FoodTruck() {
		
	}
	
	public FoodTruck(String name, String location) {
		this.applicant = name;
		this.location = location;
	}
	
	@Override
	public String getName() {
		return this.applicant;
	}
	
	public void setName(String name) {
		this.applicant = name;
	}
	
	@Override
	public String getLocation() {
		return this.location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
}
