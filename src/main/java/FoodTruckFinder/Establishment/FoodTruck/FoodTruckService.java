package main.java.FoodTruckFinder.Establishment.FoodTruck;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;

import main.java.FoodTruckFinder.Establishment.Establishment;
import main.java.FoodTruckFinder.Establishment.EstablishmentHTTPService;
import main.java.FoodTruckFinder.Utils.Helper;

public class FoodTruckService implements EstablishmentHTTPService {	
	
	@Override
	public String fetchData(String day, String time) {
		String baseURL = "https://data.sfgov.org/resource/jjew-r69b.json?";
		String dayParam = "&dayofweekstr=" + day;
		String startTimeParam = "&$where=start24" + Helper.encodeValue("<='") + time;
		String endTimeParam = Helper.encodeValue("' and ") + "end24" + Helper.encodeValue(">='") + time + Helper.encodeValue("'");
		String orderParam = "&$order=applicant";
		
		String finalUrl = baseURL + dayParam + startTimeParam + endTimeParam + orderParam;
		StringBuilder result = new StringBuilder();
		URL url;
		try {
			url = new URL(finalUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			rd.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return result.toString();
	}

	@Override
	public List<Establishment> parseResponse(String response) {
		FoodTruck[] jsonToFoodTruckArr = new Gson().fromJson(response, FoodTruck[].class);
		return Arrays.asList(jsonToFoodTruckArr);
	}
}
