package main.java.FoodTruckFinder.Establishment;

import java.util.List;

public interface EstablishmentHTTPService {
	public String fetchData(String day, String time);
	public List<Establishment> parseResponse(String response);
}
