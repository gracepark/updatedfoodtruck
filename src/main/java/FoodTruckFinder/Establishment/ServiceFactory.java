package main.java.FoodTruckFinder.Establishment;

import main.java.FoodTruckFinder.Establishment.FoodTruck.FoodTruckService;
import main.java.FoodTruckFinder.Establishment.Restaurant.RestaurantService;

public class ServiceFactory {
	public EstablishmentHTTPService getService(EstablishmentType establishmentType) {
		if (establishmentType.equals(EstablishmentType.FOODTRUCK)) {
			return new FoodTruckService();
		} else if (establishmentType.equals(EstablishmentType.RESTAURANT)) {
			return new RestaurantService();
		}
		return null;
	}
}
