package main.java.FoodTruckFinder.Establishment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import main.java.FoodTruckFinder.Utils.Helper;

public class PrintEstablishmentList {
	public static void printEstablishmentList(String day, String time, List<EstablishmentType> userInputTypes) {		
		System.out.println(Helper.centerString(96, "OPEN FOOD TRUCKS TODAY AT: " + time));
		
		// Get user input to continue or exit
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		ServiceFactory serviceFactory = new ServiceFactory();
		List<Establishment> eList = new ArrayList<>();
		
		for (EstablishmentType type : userInputTypes) {
			EstablishmentHTTPService service = serviceFactory.getService(type);
			String response = service.fetchData(day, time);
			eList.addAll(service.parseResponse(response));
		}
		System.out.println(eList);
		String userInput = "";
		
		int totalEstablishments = eList.size();
		int page = 1;
		int offset = 0;
		
		// Prints the next 10 food trucks if the user presses enter
		while (!userInput.equalsIgnoreCase("exit")) {
			for (int i = 0; i < 96; i++) System.out.print("*");
			System.out.print("\n" + Helper.centerString(78, "FOOD TRUCK NAME"));
			System.out.print(Helper.centerString(18, "ADDRESS") + "\n");
			for (int i = 0; i < 96; i++) System.out.print("*");
			
			Helper.printNextTen(offset, totalEstablishments, eList);
			
			System.out.println(Helper.centerString(96, "PAGE " + page));
			page++;
			
			if (offset + 10 >= totalEstablishments) break;
			System.out.println("\nPress enter to see the next 10 open food trucks. Enter 'exit' to quit.\n");
			try {
				userInput = br.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			// User wants to continue, increment offset by 10
			if (userInput == null || userInput.length() == 0 || userInput.trim().equals("")) {
				offset += 10;
			} 			
		}
		if (userInput.equalsIgnoreCase("exit")) {
			System.out.println("\n Exiting...");
		} else {
			System.out.println("\n No open food trucks. Exiting...");
		}
		System.exit(0);
	}
}
