package main.java.FoodTruckFinder.Establishment;

public interface Establishment {	
	public String getName();
	public String getLocation();
}
