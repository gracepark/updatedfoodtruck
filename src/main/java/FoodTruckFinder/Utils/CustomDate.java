package main.java.FoodTruckFinder.Utils;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Locale;

public class CustomDate {
	private ZonedDateTime datetime;
	private String day;
	private String time;
	
	public CustomDate() {
		this.datetime = ZonedDateTime.now(ZoneId.of("US/Pacific"));
		setDay(datetime);
		setTime(datetime);
	}

	public String getDay() {
		return this.day;
	}

	public void setDay(ZonedDateTime datetime) {
		this.day = datetime.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.ENGLISH);
	}

	public String getTime() {
		return this.time;
	}

	public void setTime(ZonedDateTime datetime) {
		this.time = DateTimeFormatter.ofPattern("HH:mm").format(datetime);
	}
}
