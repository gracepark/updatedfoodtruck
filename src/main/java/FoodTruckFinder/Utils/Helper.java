package main.java.FoodTruckFinder.Utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

import main.java.FoodTruckFinder.Establishment.Establishment;

public class Helper {
	public static String encodeValue(String value) {
        try {
            return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex.getCause());
        }
    }
	
	public static String capitalizeWord(String str) {
		str = str.toLowerCase();
        char[] letters = str.toCharArray();
        for (int i = 0; i < letters.length; i++) {
            if (i == 0 || letters[i-1] == ' ') {
                letters[i] = Character.toUpperCase(letters[i]);
            }
        }
        return String.valueOf(letters);
	}
	
	public static void printNextTen(int start, int responseLength, List<Establishment> establishmentList) {
		System.out.println();
		int end = (start + 10 >= responseLength) ? responseLength : start + 10;

		// Print out food truck name and it's location
		for (int i = start; i < end; i++) {
			Establishment est = establishmentList.get(i);
			System.out.println(String.format("%-75s %-35s", capitalizeWord(est.getName()), capitalizeWord(est.getLocation())));
		}
	}

	public static String centerString (int width, String s) {
	    return String.format("%-" + width  + "s", String.format("%" + (s.length() + (width - s.length()) / 2) + "s", s));
	}
}
